(function() {
  'use strict';

  angular
    .module('koapp-page-loader', [])
    .directive('koappPageLoader', function() {
    return {
      restrict: 'E',
      template: '<div class="loadingView" ng-show="isBusy"><div class="spinner"></div></div>'
    };
    });
}());

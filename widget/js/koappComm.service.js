angular
    .module('firebaseEcommerce.items.controller')
    .service('koappCommService', koappCommService);

function koappCommService(){

  return {
    getData: getData
  };

  function getData(fn) {
    koappComm.iframe.ready();
    koappComm.iframe.onData(fn);
  }

}

angular
    .module('firebaseEcommerce.items.controller', [])
    .controller('itemsController', itemsController);

itemsController.$inject = ['$q', '$scope', '$translatePartialLoader', '$translate', '$location', 'koappCommService',
                           'firebaseConnectionService', 'manageFirebaseService', 'catalogService', 'firebaseStorageService'];

function itemsController($q, $scope, $translatePartialLoader, $translate, $location, koappCommService,
                         firebaseConnectionService, manageFirebaseService, catalogService, firebaseStorageService) {

  var dataFromConfig = {};

  $scope.translateLoaded = false;
  $scope.itemList        = [];
  $scope.idSelected      = '';
  $scope.isBusy          = true;
  $scope.showDataError   = false;

  $scope.keepItem        = keepItem;
  $scope.goTo            = goTo;
  $scope.deleteItem      = deleteItem;
  $scope.deletionIsOk    = false;
  $scope.deletionIsKo    = false;
  $scope.noItems         = false;
  $scope.itemsLoaded     = false;

  $translatePartialLoader.addPart('items');
  $translate.refresh();

  init();

  function init() {
    koappCommService.getData(getModuleData);
  }

  function getModuleData(data) {
    dataFromConfig = data;
    koappComm.iframe.close();
    if (dataFromConfig._id && dataFromConfig.lang && dataFromConfig.accessToken) {
      $translate.use(dataFromConfig.lang);
      $scope.translateLoaded = true;
      setFirebaseConnection();
    }
  }

  function setFirebaseConnection() {
    var dataConfig = dataFromConfig.plugin.scope;
    firebaseConnectionService.setFirebaseSource(dataConfig.firebaseUrl, dataConfig.firebaseStorage, dataConfig.firebaseApiKey, true, dataConfig.adminUser, dataConfig.adminPsswd)
    .then(getFirebaseData)
    .catch(errorLog);
  }

  function getFirebaseData(data) {
    $scope.itemList = [];
    manageFirebaseService.getAll(dataFromConfig.plugin.scope.itemsCollection)
    .then(workWithFirebaseItems)
    .catch(errorLog);
  }

  function workWithFirebaseItems(firebaseItems) {
    if (!firebaseItems.length) {
      $scope.noItems     = true;
      $scope.itemsLoaded = false;
      $scope.isBusy      = false;
    } else {
      $scope.noItems     = false;
      $scope.itemsLoaded = true;
      angular.forEach(firebaseItems, prepareItemsList);
    }
  }

  function prepareItemsList(item) {
    var promises   = [];
    $scope.noItems = false;
    if(item && item.sku) {
      imgArray = [];
      angular.forEach(item.images, function(value, key) {
        prepareImgArray(value, key, promises);
      });

      $q.all(promises)
      .then(function(urls) {
        item.images   = urls;
        $scope.itemList.push(item);
        $scope.isBusy = false;
      })
      .catch(function() {
        $scope.itemList.push(item);
        $scope.isBusy = false;
      });
    }

  }

  function prepareImgArray(value, key, promises) {
    var deferred = $q.defer();
    firebaseStorageService.getImages(value)
    .then(function(url) {
      deferred.resolve(url);
    })
    .catch(function() {
      deferred.reject(false);
    })
    promises.push(deferred.promise);
  }

  function deleteItem(id) {
    $scope.isBusy = true;
    $('#deleteSafe').modal('hide');
    catalogService.removeItem(id, dataFromConfig.plugin.scope.itemsCollection)
    .then(function() {
      showAlertModal(true);
    })
    .catch(function() {
      showAlertModal(false);
    });

  }

  function showAlertModal(isOk) {
    (isOk) ? toogleDeletion(true, false) : toogleDeletion(false, true);
    getFirebaseData();
  }

  function toogleDeletion(isOk, isKo) {
    $scope.deletionIsOk = isOk;
    $scope.deletionIsKo = isKo;
  }

  function keepItem(id) {
    $scope.idSelected = id;
  }

  function goTo(path) {
    $location.path(path);
  }

  function errorLog(error) {
    $scope.showDataError = true;
    $scope.isBusy        = false;
    console.log('Error -> ', error);
  }

}

angular.module('firebaseEcommerce', [
  'ngRoute',
  'pascalprecht.translate',
  'firebaseEcommerce.router.config',
  'firebaseEcommerce.error.controller',
  'firebaseEcommerce.items.controller',
  'firebaseEcommerce.edit.controller',
  'firebaseEcommerce.sales.controller',
  'firebaseEcommerce.translate.config',
  'firebaseEcommerce.edit.controller.appFilereader',
  'firebase.services',
  'koapp-page-loader'
]);

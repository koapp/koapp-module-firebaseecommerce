(function() {
  'use strict';
  angular
    .module('firebaseEcommerce.router.config', ['ngRoute'])
    .config(setDefaultPaths);
    setDefaultPaths.$inject = ['$routeProvider'];

    function setDefaultPaths($routeProvider) {
      $routeProvider
      .when('/items', {
        templateUrl: 'items.html',
        controller: 'itemsController'})
      .when('/edit', {
        templateUrl: 'edit.html',
        controller: 'editController'
      })
      .when('/edit/:idItem', {
        templateUrl: 'edit.html',
        controller: 'editController'
      })
      .when('/add', {
        templateUrl: 'edit.html',
        controller: 'editController'
      })
      .when('/sales', {
        templateUrl: 'sales.html',
        controller: 'salesController'
      })
      .when("/error", {
        templateUrl: "error.html",
        controller: 'errorController'
      })
      .otherwise({
        redirectTo: '/error'
      });
    }
}());

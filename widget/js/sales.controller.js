angular
    .module('firebaseEcommerce.sales.controller', [])
    .controller('salesController', salesController);

salesController.$inject = ['$q','$scope', '$translatePartialLoader', '$translate', 'koappCommService', 'manageFirebaseService'];

function salesController($q, $scope, $translatePartialLoader, $translate, koappCommService, manageFirebaseService) {

  $scope.translateLoaded = false;
  $scope.saleList        = [];
  $scope.noSales         = false;
  $scope.salesLoaded     = false;
  $scope.isBusy          = true;

  $translatePartialLoader.addPart('sales');
  $translate.refresh();

  var dataFromConfig = {};

  init();

  function init() {
    koappCommService.getData(getModuleData);
  }

  function getModuleData(data) {
    dataFromConfig = data;
    if(dataFromConfig._id && dataFromConfig.lang && dataFromConfig.accessToken) {
      $translate.use(dataFromConfig.lang);
      $scope.translateLoaded = true;
      getFirebaseData();
    }
  }

  function getFirebaseData(data) {
    $scope.saleList = [];

    manageFirebaseService.getAll(dataFromConfig.plugin.scope.salesCollection)
    .then(function(firebaseSales) {
      getSales(firebaseSales);
    })
    .catch(errorLog);
  }

  function getSales(firebaseSales) {
    if (firebaseSales.length === 0) {
      $scope.noSales     = true;
      $scope.isBusy      = false;
      $scope.salesLoaded = false;
    } else {
      $scope.noSales     = false;
      $scope.salesLoaded = true;
    }
    angular.forEach(firebaseSales, prepareSalesList);
    $scope.isBusy = false;
  }

  function prepareSalesList(sale) {
    if(sale && sale.name) $scope.saleList.push(sale);
  }

  function errorLog(error) {
    console.log('Error -> ', error);
    $scope.isBusy = false;
  }

}

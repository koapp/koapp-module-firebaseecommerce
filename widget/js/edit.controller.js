angular
    .module('firebaseEcommerce.edit.controller', [])
    .controller('editController', editController);

editController.$inject = ['$q','$scope', '$translatePartialLoader', '$translate', '$routeParams', '$location',
                          '$filter', 'koappCommService', 'manageFirebaseService', 'firebaseStorageService', 'catalogService'];

function editController($q, $scope, $translatePartialLoader, $translate, $routeParams, $location,
                        $filter, koappCommService, manageFirebaseService, firebaseStorageService, catalogService) {

  var idParameter = $routeParams.idItem || null;

  $translatePartialLoader.addPart('edit');
  $translate.refresh();

  var dataFromConfig   = {};
  $scope.locationState = '';
  $scope.imageArray    = [];
  $scope.isBusy        = true;

  $scope.goTo     = goTo;
  $scope.pushData = pushData;
  $scope.delItem  = delItem;

  init();

  function init() {
    $scope.dataItem = {};
    koappCommService.getData(getModuleData);
  }

  function getModuleData(data) {
    (idParameter) ? $scope.locationState = $filter('translate')('edit-breadcrumb-edit') : $scope.locationState = $filter('translate')('edit-breadcrumb-new');
    dataFromConfig = data;
    if(dataFromConfig._id && dataFromConfig.lang && dataFromConfig.accessToken) {
      $translate.use(dataFromConfig.lang);
      $scope.translateLoaded = true;
      (idParameter) ? getFirebaseData() : $scope.isBusy = false;
      (!$scope.dataItem.currencySymbol) ? (dataFromConfig.lang === 'es_ES') ? $scope.dataItem.currencySymbol = '€' :
                                                                              $scope.dataItem.currencySymbol = '$' : false;
    }
  }

  function getFirebaseData(data) {
    $scope.itemList = [];
    manageFirebaseService.getAll(dataFromConfig.plugin.scope.itemsCollection)
    .then(searchData)
    .catch(errorLog);
  }

  function searchData(firebaseData) {
    angular.forEach(firebaseData, searchItemSelected);
  }

  function searchItemSelected(item) {
    if (item.$id === idParameter) {

      var promises = [];
      $scope.dataItem = item;
      angular.forEach($scope.dataItem.images, function(value, key) {
        prepareImgArray(value, key, promises);
      });

      $q.all(promises)
      .then(function(urls) {
        $scope.dataItem.images = urls;
        $scope.isBusy = false;
      })
      .catch(function() {
        $scope.isBusy = false;
      });

    }
  }

  function prepareImgArray(value, key, promises) {
    var deferred = $q.defer();

    firebaseStorageService.getImages(value).then(function(url) {
      deferred.resolve(url);
    })
    .catch(function() {
      deferred.reject();
    });
    promises.push(deferred.promise);
  }

  function goTo(path) {
    $location.path(path);
  }

  function pushData() {
    $scope.errorValidation = false;
    $scope.actionCompleted = false;
    $scope.actionFailed    = false;
    var countErrors        = 0;
    validateForm(countErrors);
  }

  function saveData() {
    var images64 = {};

    if(Array.isArray($scope.dataItem.images)) angular.forEach($scope.dataItem.images, function(value, key) {
      createImages64(value, key, images64)
    });

    ($scope.dataItem.$id) ? setOperative(images64) : pushOperative(images64);

  }

  function createImages64(value, key, images64) {
    images64['img' + (key+1)]     = {};
    images64['img' + (key+1)].url = value;
  }

  function successOperative(slug) {
    $scope.successAction   = $filter('translate')(slug);
    $scope.actionCompleted = true;
    init();
  }

  function failOperative(slug) {
    $scope.errorSlug     = $filter('translate')(slug);
    $scope.actionFailed  = true;
    init();
  }

  function setOperative(images64) {
    $scope.isBusy  = true;
    var imagesPath = {};

    angular.forEach(images64, function(value, key) {
      createImagesPath(value, key, imagesPath);
    });

    $scope.dataItem.images = imagesPath;
    firebaseStorageService.pushImages($scope.dataItem.$id, images64)
    .then(successImagePush)
    .catch(function() {
      failOperative('push-fail-images');
    });
  }

  function pushOperative(images64) {
    catalogService.pushItem($scope.dataItem, dataFromConfig.plugin.scope.itemsCollection, images64)
    .then(function() {
      successOperative('edit-succes-push');
    })
    .catch(function() {
      failOperative('edit-fail-push');
    });

  }

  function createImagesPath(value, key, imagesPath) {
    imagesPath[key] = 'images/' + $scope.dataItem.$id + '/' + key;
  }

  function successImagePush() {
    catalogService.setItem($scope.dataItem, dataFromConfig.plugin.scope.itemsCollection)
    .then(function() {
      successOperative('edit-succes-set');
    })
    .catch(function() {
      failOperative('edit-fail-set');
    });
  }

  function validateForm(countErrors) {
    var hasError      = false;
    $scope.dataErrors = {};
    allFields = ['name', 'category', 'description', 'images', 'price', 'sku', 'taxes'];
    for(var i = 0; i<7; i++) {
      hasError = checkData(allFields[i]);
      if (hasError) countErrors++;
    }
    if (countErrors === 0) {
      (!$scope.dataItem.$id) ? isSkuDuplicated() : saveData();
    } else {
      $scope.errorValidation = true;
      $scope.errorSlug       = $filter('translate')('edit-error-emptydata');
    }
  }

  function checkData(field) {
    if(!$scope.dataItem[field]) $scope.dataErrors[field] = true;
    return (!$scope.dataItem[field]);
  }

  function isSkuDuplicated() {
    catalogService.checkRecord($scope.dataItem, dataFromConfig.plugin.scope.itemsCollection)
    .then(setSkuData)
    .catch(saveData);

  }

  function setSkuData() {
    $scope.errorSlug       = $filter('translate')('edit-error-skuduplicated');
    $scope.dataErrors.sku  = true;
    $scope.errorValidation = true;
  }

  function delItem(idToDel) {
    var array = angular.copy($scope.dataItem.images);
    array.splice(idToDel, 1);
    $scope.dataItem.images = array;
  }

  function errorLog() {
    console.log('errorLog -> ', errorLog);
  }
}

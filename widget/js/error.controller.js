angular
    .module('firebaseEcommerce.error.controller', [])
    .controller('errorController', errorController);

errorController.$inject = ['$q','$scope', '$translatePartialLoader', '$translate'];

function errorController($q, $scope, $translatePartialLoader, $translate) {

  $translatePartialLoader.addPart('error');
  $translate.refresh();

  var data = {};

  koappComm.iframe.ready();
  koappComm.iframe.onData(function(data) {
    if(data._id && data.lang && data.accessToken) {
      $translate.use(data.lang);
      $translate.refresh();
    }
  });

}

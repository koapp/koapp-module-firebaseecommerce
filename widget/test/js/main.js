var configData = '';

function clickSendReady() {
  koappComm.main.onReady();
  document.getElementById('console').appendChild(createTxt('Iframe ready to receive information')).setAttribute('id', 'readyInfo');
}

function clickSendData() {
  var widgetContent = document.getElementById('widgetContent').contentWindow;
  document.getElementById('console').appendChild(createTxt('Information sent to Iframe.')).setAttribute('id', 'sendDataEvent');
  koappComm.main.sendData(widgetContent, configData);
}

function createTxt(text, pColor) {
  var newText = document.createTextNode(text);
  var newP    = document.createElement('P');
  (pColor) ? newP.style.color = pColor : false;
  newP.appendChild(newText);
  return newP;
}

function setExtraDataForm() {
  var urlData = document.getElementById('urlInput').value;
  configData  = JSON.parse(document.getElementById('dataToSend').value);
  document.getElementById('widgetContent').setAttribute('src', urlData);
  document.getElementById('console').appendChild(createTxt('Extra data added to DOM.')).setAttribute('id', 'extraDataDom');
}

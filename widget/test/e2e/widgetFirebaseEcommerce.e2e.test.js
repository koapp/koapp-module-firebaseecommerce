(function() {

	'use strict';

	var path = require('path');

	var helper = require('../../../../../../e2e-tests/e2etest.service');

  describe('firebaseecommerce controller', function() {

		var ptor;

		var dataConfig = {
			"_id": "5892f3e8ecd3a6b83201abf1",
			"name": "storage",
			"packageName": "com.kingofapp.SBPZfEeBecdEaLbBEZOIabfI",
			"isCompiled": [],
			"plugin": {
				"_id": "5853dda705f948937d201b86",
				"name": "Firebase eCommerce",
				"identifier": "firebaseecommerce",
				"descriptionShort": {
					"es-ES": "Muestra una lista de todos los productos en stock y su precio.",
					"en-US": "It shows a list of all products in stock and their price."
				},
				"icon": "room",
				"type": "A",
				"version": "0.0.1",
				"author": "King of app",
				"category": [
					"module",
					"ecommerce",
					"firebase"
				],
				"price": 0,
				"subscription": false,
				"canContain": false,
				"platforms": [
					"android",
					"ios",
					"windows"
				],
				"view": "http://localhost:9001/modules/firebaseecommerce/index.html",
				"files": [
					"http://localhost:9001/modules/firebaseecommerce/controller.js",
					"http://localhost:9001/modules/firebaseecommerce/services/firebaseConnection.service.js",
					"http://localhost:9001/modules/firebaseecommerce/services/firebaseStorage.service.js",
					"http://localhost:9001/modules/firebaseecommerce/services/itemsDb.service.js",
					"http://localhost:9001/modules/firebaseecommerce/services/manageDb.service.js",
					"http://localhost:9001/modules/firebaseecommerce/services/salesDb.service.js",
					"http://localhost:9001/modules/firebaseecommerce/style.html"
				],
				"requires": {
					"out": [
						"firebaseecommercedetail"
					]
				},
				"scope": {
					"firebaseApiKey": "AIzaSyDxVM08OKkfCtSNCUPMnnKNx2VaOOazXdY",
					"firebaseUrl": "https://fir-ecommercetests.firebaseio.com/",
					"firebaseStorage": "gs://fir-ecommercetests.appspot.com",
					"itemsCollection": "items",
					"salesCollection": "sales",
					"adminUser": "koappfirebasetests@gmail.com",
					"adminPsswd": "Firebaseserver123",
					"childrenUrl": {
						"firebaseecommercedetail": "/polymermenu-X6JOG/firebaseecommercedetail-4thQt"
					},
					"path": "/polymermenu-X6JOG/firebaseecommercedetail-4thQt"
				},
				"widget": {
					"title": "Admin - Firebase eCommerce",
					"url": "http://localhost:9001/modules/firebaseecommerce/widget/index.html#/items"
				},
				"libs": [
					{
						"bower": {
							"firebase": "^3.6.4"
						},
						"src": "bower_components/firebase/firebase.js"
					},
					{
						"bower": {
							"angularfire": "^2.2.0"
						},
						"src": "bower_components/angularfire/dist/angularfire.min.js"
					},
					{
						"bower": {
							"koapp-widget-communicator": "^1.1.1"
						},
						"src": "bower_components/koapp-widget-communicator/main.js"
					},
					{
						"bower": {
							"bootstrap": "^3.3.5"
						},
						"src": "bower_components/bootstrap/dist/js/bootstrap.min.js"
					},
					{
						"bower": {
							"jquery": "^2.x"
						},
						"src": "bower_components/jquery/dist/jquery.min.js"
					},
					{
						"bower": {
							"font-awesome": "^4.7.0"
						},
						"src": "bower_components/font-awesome/css/font-awesome.min.css"
					},
					{
						"bower": {
							"moment": "^2.17.1"
						},
						"src": "bower_components/moment/moment.js"
					}
				],
				"config": [
					{
						"templateOptions": {
							"label": "Firebase API Key",
							"required": true,
							"description": "Firebase API Key"
						},
						"type": "input",
						"key": "firebaseApiKey"
					},
					{
						"templateOptions": {
							"label": "Firebase db URL",
							"required": true,
							"description": "Firebase db URL"
						},
						"type": "input",
						"key": "firebaseUrl"
					},
					{
						"templateOptions": {
							"label": "Firebase storage",
							"required": true,
							"description": "Firebase storage id"
						},
						"type": "input",
						"key": "firebaseStorage"
					},
					{
						"templateOptions": {
							"label": "Items collection name",
							"required": true,
							"description": "Firebase Items collection in db"
						},
						"type": "input",
						"key": "itemsCollection"
					},
					{
						"templateOptions": {
							"label": "Sales collection name",
							"required": true,
							"description": "Firebase Sales collection in db"
						},
						"type": "input",
						"key": "salesCollection"
					},
					{
						"templateOptions": {
							"label": "Email from firebase database administrator",
							"required": true,
							"description": "Email from firebase database administrator"
						},
						"type": "input",
						"key": "adminUser"
					},
					{
						"templateOptions": {
							"label": "Password from firebase database administrator",
							"required": true,
							"description": "Password from firebase database administrator"
						},
						"type": "input",
						"key": "adminPsswd"
					}
				],
				"showOn": {
					"menu": true,
					"market": true,
					"dragDrop": true
				},
				"events": {
					"require": [
						"payment"
					],
					"provide": []
				},
				"images": {
					"list": "http://localhost:9001/modules/firebaseecommerce/images/list.png",
					"screenshots": [
						"http://localhost:9001/modules/firebaseecommerce/images/screenshot01.png"
					],
					"popover": "http://localhost:9001/modules/firebaseecommerce/images/popover.png",
					"banner": "http://localhost:9001/modules/firebaseecommerce/images/banner.png",
					"logo": "http://localhost:9001/modules/firebaseecommerce/images/logo.png"
				},
				"createdAt": "2016-12-16T12:27:34.965Z",
				"uniqueId": "firebaseecommerce-DG0tm",
				"itemType": "item",
				"pose": 0,
				"path": "/polymermenu-X6JOG/firebaseecommerce-DG0tm"
			},
			"accessToken": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJfaWQiOiI1ODdhMDkyYjVlMjhmMjUwOGUwMDZhN2IiLCJ0eXBlIjoidXNlciIsImxhbmciOiJlbi1VUyIsInBhc3N3b3JkIjoiYTc2MGJhNDM3YWI3ZGJiMDFjYmI0ZDYxNTYwNWMwM2Y1YzFiMzFlOCIsImVtYWlsIjoiZGF2aWQuYXIxOTgyQGdtYWlsLmNvbSIsIm5hbWUiOnsiZmlyc3QiOiJEYXZpZCIsImxhc3QiOiJBcm9jaGVuYSJ9LCJwcm9maWxlcyI6eyJtYWluIjp7ImFsaWFzIjoiTWFpbiIsImNvbXBhbnkiOiJEYXZpZCBBcm9jaGVuYSIsImFkZHJlc3MiOiJkYXNhc2QiLCJjaXR5IjoiZHNmc2QiLCJ0YXhJZCI6ImRzZmZzZCIsInBob25lIjoiKzM0NjU0MzUzNTM0IiwiemlwQ29kZSI6ImZkc2RmcyIsInN0YXRlIjoiZGZzZGZzIiwiY291bnRyeSI6IkFEIiwicGhvbmVFcnJvciI6InByb2ZpbGVzRm9ybS5lcnJvci5waG9uZSJ9fSwiY3JlYXRlQXQiOiIyMDE3LTAxLTE0VDExOjE5OjA3Ljg0NFoiLCJpc0FjdGl2ZSI6dHJ1ZSwiZW1haWxWZXIiOiIyYTQ0MDM1MC0zNzlkLTQ4YmItOWI3YS0yZGFhZmVkMTE1NTAiLCJjb21waWxhdGlvbiI6eyJhdmFpbGFibGUiOjAsImV4cGlyYWJsZSI6MCwicmVuZXdUb3RhbCI6MCwicmVuZXdEYXRlIjpudWxsfSwib2xkRW1haWxzIjpbImRhdmlkQGtpbmdvZmFwcC5lcyIsImRhdmlkLmFyMTk4MkBnbWFpbC5jb20iLCJkYXZpZC5hcjE5ODJAa2luZ29mYXBwLmVzIiwiZGF2aWQuYXIxOTgyQGdtYWlsLmNvbSIsImRhdmlkQGtpbmdvZmFwcC5lcyIsImRhdmlkLmFyMTk4MkBnbWFpbC5jb20iLCJkYXZpZEBraW5nb2ZhcHAuZXMiXSwicmVjb3ZlcnlUb2tlbiI6bnVsbCwiZXhwaXJlVG9rZW4iOm51bGwsInVwZGF0ZWRBdCI6IjIwMTctMDMtMjdUMDg6MDA6MDcuODc0WiIsInRvQnV5Ijp7ImNvbXBpbGF0aW9ucyI6W3sibmFtZSI6IjEgQXBwIGNvbXBpbGF0aW9uIiwiaWRlbnRpZmllciI6ImJvbmQwMSIsIm5BcHBzIjoxLCJwcmljZSI6MTB9XX0sImJvdWdodCI6eyJjb21waWxhdGlvbnMiOlt7fV19LCJub0NoZWNrIjpmYWxzZSwiaWF0IjoxNDkzMDIyNDQzLCJleHAiOjE0OTMwNTg0NDN9.1NfnaamLdclK5HZFsAp3IAbxcUBKHPJuX3AVFLIV2wo",
			"lang": "en_US"
		};

		var dataConfigNoFirebaseUrl = {
			"_id": "5892f3e8ecd3a6b83201abf1",
			"name": "storage",
			"packageName": "com.kingofapp.SBPZfEeBecdEaLbBEZOIabfI",
			"isCompiled": [],
			"plugin": {
				"_id": "5853dda705f948937d201b86",
				"name": "Firebase eCommerce",
				"identifier": "firebaseecommerce",
				"descriptionShort": {
					"es-ES": "Muestra una lista de todos los productos en stock y su precio.",
					"en-US": "It shows a list of all products in stock and their price."
				},
				"icon": "room",
				"type": "A",
				"version": "0.0.1",
				"author": "King of app",
				"category": [
					"module",
					"ecommerce",
					"firebase"
				],
				"price": 0,
				"subscription": false,
				"canContain": false,
				"platforms": [
					"android",
					"ios",
					"windows"
				],
				"view": "http://localhost:9001/modules/firebaseecommerce/index.html",
				"files": [
					"http://localhost:9001/modules/firebaseecommerce/controller.js",
					"http://localhost:9001/modules/firebaseecommerce/services/firebaseConnection.service.js",
					"http://localhost:9001/modules/firebaseecommerce/services/firebaseStorage.service.js",
					"http://localhost:9001/modules/firebaseecommerce/services/itemsDb.service.js",
					"http://localhost:9001/modules/firebaseecommerce/services/manageDb.service.js",
					"http://localhost:9001/modules/firebaseecommerce/services/salesDb.service.js",
					"http://localhost:9001/modules/firebaseecommerce/style.html"
				],
				"requires": {
					"out": [
						"firebaseecommercedetail"
					]
				},
				"scope": {
					"firebaseApiKey": "AIzaSyDxVM08OKkfCtSNCUPMnnKNx2VaOOazXdY",
					"firebaseUrl": "",
					"firebaseStorage": "gs://fir-ecommercetests.appspot.com",
					"itemsCollection": "items",
					"salesCollection": "sales",
					"adminUser": "koappfirebasetests@gmail.com",
					"adminPsswd": "Firebaseserver123",
					"childrenUrl": {
						"firebaseecommercedetail": "/polymermenu-X6JOG/firebaseecommercedetail-4thQt"
					},
					"path": "/polymermenu-X6JOG/firebaseecommercedetail-4thQt"
				},
				"widget": {
					"title": "Admin - Firebase eCommerce",
					"url": "http://localhost:9001/modules/firebaseecommerce/widget/index.html#/items"
				},
				"libs": [
					{
						"bower": {
							"firebase": "^3.6.4"
						},
						"src": "bower_components/firebase/firebase.js"
					},
					{
						"bower": {
							"angularfire": "^2.2.0"
						},
						"src": "bower_components/angularfire/dist/angularfire.min.js"
					},
					{
						"bower": {
							"koapp-widget-communicator": "^1.1.1"
						},
						"src": "bower_components/koapp-widget-communicator/main.js"
					},
					{
						"bower": {
							"bootstrap": "^3.3.5"
						},
						"src": "bower_components/bootstrap/dist/js/bootstrap.min.js"
					},
					{
						"bower": {
							"jquery": "^2.x"
						},
						"src": "bower_components/jquery/dist/jquery.min.js"
					},
					{
						"bower": {
							"font-awesome": "^4.7.0"
						},
						"src": "bower_components/font-awesome/css/font-awesome.min.css"
					},
					{
						"bower": {
							"moment": "^2.17.1"
						},
						"src": "bower_components/moment/moment.js"
					}
				],
				"config": [
					{
						"templateOptions": {
							"label": "Firebase API Key",
							"required": true,
							"description": "Firebase API Key"
						},
						"type": "input",
						"key": "firebaseApiKey"
					},
					{
						"templateOptions": {
							"label": "Firebase db URL",
							"required": true,
							"description": "Firebase db URL"
						},
						"type": "input",
						"key": "firebaseUrl"
					},
					{
						"templateOptions": {
							"label": "Firebase storage",
							"required": true,
							"description": "Firebase storage id"
						},
						"type": "input",
						"key": "firebaseStorage"
					},
					{
						"templateOptions": {
							"label": "Items collection name",
							"required": true,
							"description": "Firebase Items collection in db"
						},
						"type": "input",
						"key": "itemsCollection"
					},
					{
						"templateOptions": {
							"label": "Sales collection name",
							"required": true,
							"description": "Firebase Sales collection in db"
						},
						"type": "input",
						"key": "salesCollection"
					},
					{
						"templateOptions": {
							"label": "Email from firebase database administrator",
							"required": true,
							"description": "Email from firebase database administrator"
						},
						"type": "input",
						"key": "adminUser"
					},
					{
						"templateOptions": {
							"label": "Password from firebase database administrator",
							"required": true,
							"description": "Password from firebase database administrator"
						},
						"type": "input",
						"key": "adminPsswd"
					}
				],
				"showOn": {
					"menu": true,
					"market": true,
					"dragDrop": true
				},
				"events": {
					"require": [
						"payment"
					],
					"provide": []
				},
				"images": {
					"list": "http://localhost:9001/modules/firebaseecommerce/images/list.png",
					"screenshots": [
						"http://localhost:9001/modules/firebaseecommerce/images/screenshot01.png"
					],
					"popover": "http://localhost:9001/modules/firebaseecommerce/images/popover.png",
					"banner": "http://localhost:9001/modules/firebaseecommerce/images/banner.png",
					"logo": "http://localhost:9001/modules/firebaseecommerce/images/logo.png"
				},
				"createdAt": "2016-12-16T12:27:34.965Z",
				"uniqueId": "firebaseecommerce-DG0tm",
				"itemType": "item",
				"pose": 0,
				"path": "/polymermenu-X6JOG/firebaseecommerce-DG0tm"
			},
			"accessToken": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJfaWQiOiI1ODdhMDkyYjVlMjhmMjUwOGUwMDZhN2IiLCJ0eXBlIjoidXNlciIsImxhbmciOiJlbi1VUyIsInBhc3N3b3JkIjoiYTc2MGJhNDM3YWI3ZGJiMDFjYmI0ZDYxNTYwNWMwM2Y1YzFiMzFlOCIsImVtYWlsIjoiZGF2aWQuYXIxOTgyQGdtYWlsLmNvbSIsIm5hbWUiOnsiZmlyc3QiOiJEYXZpZCIsImxhc3QiOiJBcm9jaGVuYSJ9LCJwcm9maWxlcyI6eyJtYWluIjp7ImFsaWFzIjoiTWFpbiIsImNvbXBhbnkiOiJEYXZpZCBBcm9jaGVuYSIsImFkZHJlc3MiOiJkYXNhc2QiLCJjaXR5IjoiZHNmc2QiLCJ0YXhJZCI6ImRzZmZzZCIsInBob25lIjoiKzM0NjU0MzUzNTM0IiwiemlwQ29kZSI6ImZkc2RmcyIsInN0YXRlIjoiZGZzZGZzIiwiY291bnRyeSI6IkFEIiwicGhvbmVFcnJvciI6InByb2ZpbGVzRm9ybS5lcnJvci5waG9uZSJ9fSwiY3JlYXRlQXQiOiIyMDE3LTAxLTE0VDExOjE5OjA3Ljg0NFoiLCJpc0FjdGl2ZSI6dHJ1ZSwiZW1haWxWZXIiOiIyYTQ0MDM1MC0zNzlkLTQ4YmItOWI3YS0yZGFhZmVkMTE1NTAiLCJjb21waWxhdGlvbiI6eyJhdmFpbGFibGUiOjAsImV4cGlyYWJsZSI6MCwicmVuZXdUb3RhbCI6MCwicmVuZXdEYXRlIjpudWxsfSwib2xkRW1haWxzIjpbImRhdmlkQGtpbmdvZmFwcC5lcyIsImRhdmlkLmFyMTk4MkBnbWFpbC5jb20iLCJkYXZpZC5hcjE5ODJAa2luZ29mYXBwLmVzIiwiZGF2aWQuYXIxOTgyQGdtYWlsLmNvbSIsImRhdmlkQGtpbmdvZmFwcC5lcyIsImRhdmlkLmFyMTk4MkBnbWFpbC5jb20iLCJkYXZpZEBraW5nb2ZhcHAuZXMiXSwicmVjb3ZlcnlUb2tlbiI6bnVsbCwiZXhwaXJlVG9rZW4iOm51bGwsInVwZGF0ZWRBdCI6IjIwMTctMDMtMjdUMDg6MDA6MDcuODc0WiIsInRvQnV5Ijp7ImNvbXBpbGF0aW9ucyI6W3sibmFtZSI6IjEgQXBwIGNvbXBpbGF0aW9uIiwiaWRlbnRpZmllciI6ImJvbmQwMSIsIm5BcHBzIjoxLCJwcmljZSI6MTB9XX0sImJvdWdodCI6eyJjb21waWxhdGlvbnMiOlt7fV19LCJub0NoZWNrIjpmYWxzZSwiaWF0IjoxNDkzMDIyNDQzLCJleHAiOjE0OTMwNTg0NDN9.1NfnaamLdclK5HZFsAp3IAbxcUBKHPJuX3AVFLIV2wo",
			"lang": "en_US"
		};

		dataConfig = JSON.stringify(dataConfig);
		dataConfigNoFirebaseUrl = JSON.stringify(dataConfigNoFirebaseUrl);

		var url = '../index.html#/items';

    beforeAll(function() {
      browser.ignoreSynchronization = true;
      browser.driver.manage().window().setSize(800, 1400);
      browser.get('http://localhost:9001/modules/firebaseecommerce/widget/test/widget.e2etest.html');
    });

    it('Show works as a test', function(done) {
      helper.elementExist('#testingH1');
      done();
    });

		it('should show a message in the pre console when submit data', function(done) {
			element(by.id('urlInput')).sendKeys(url);
			element(by.id('dataToSend')).sendKeys(dataConfig);
			helper.clickElement('#submitButton');
			helper.wait(1000);
			helper.checkElementToBe('#extraDataDom', 'Extra data added to DOM.');
			done();
		})

		it('Should trigger onReady event', function(done) {
			helper.clickElement('#readyButton');
			done();
		});

		it('should show ready message in the pre console', function(done) {
			helper.checkElementToBe('#readyInfo', 'Iframe ready to receive information');
			done();
		});

		it('Should trigger sendData event', function(done) {
			helper.clickElement('#dataButton');
			done();
		});

		it('should show iframe loaded message', function(done) {
			helper.wait(5000);
			helper.checkElementToBe('#sendDataEvent', 'Information sent to Iframe.');
			done();
		});

		it('should go to sales view, check if there´s a sale and comeback to items view', function(done) {

			browser.switchTo().frame(0);
			helper.clickElement('#goToSalesView');
			browser.switchTo().defaultContent();
			helper.wait(3000);
			helper.clickElement('#dataButton');
			helper.wait(5000);
			browser.switchTo().frame(0);
			helper.checkElementToBe('#salesTitle', 'List of sales saved into the database');
			helper.checkElementToBe('.name-prod1Name', 'prod1Name');
			helper.clickElement('#goToItemsView');
			browser.switchTo().defaultContent();
			helper.clickElement('#dataButton');
			helper.wait(5000);
			browser.switchTo().frame(0);
			helper.checkElementToBe('#itemsTitle', 'List of items saved into the database');

			done();
		});

		it('should go to add view through add item button', function(done) {
			helper.clickElement('#btnAddItem');
			browser.switchTo().defaultContent();
			helper.wait(3000);
			helper.clickElement('#dataButton');
			helper.wait(5000);
			browser.switchTo().frame(0);
			helper.checkElementToBe('#locationBreadcrumb', 'New Item');
			done();
		});

		it('should show all the fields in items view', function(done) {
			helper.clickElement('#cancelButton');
			browser.switchTo().defaultContent();
			helper.clickElement('#dataButton');
			helper.wait(5000);
			browser.switchTo().frame(0);
			helper.checkStyleToContain('.url-prod1Name', 'background-image: url("https://firebasestorage.googleapis.com/v0/b/fir-ecommercetests.appspot.com/o/images%2F-KdPfbP6jr9rAfmrDARA%2Fimg1?alt=media&token=90aded89-cef5-463f-b22c-13e60561e3d8");', true)
			helper.checkElementToBe('.name-prod1Name', 'prod1Name');
			helper.checkElementToBe('.sku-prod1Name', '123');
			helper.checkElementToBe('.cat-prod1Name', 'prod1Cat');
			helper.checkElementToBe('.price-prod1Name', '100€');
			done();
		});

		it('should show the second prod in the items list', function(done) {
			helper.checkStyleToContain('.url-prod2Name', 'background-image: url("https://firebasestorage.googleapis.com/v0/b/fir-ecommercetests.appspot.com/o/images%2F-KdPfje_dD8PO90Xtn_X%2Fimg1?alt=media&token=43dc109c-eabc-4483-9319-cb590e1c63eb");', true)
			helper.checkElementToBe('.name-prod2Name', 'prod2Name');
			helper.checkElementToBe('.sku-prod2Name', '124');
			helper.checkElementToBe('.cat-prod2Name', 'prod2Cat');
			helper.checkElementToBe('.price-prod2Name', '200$');
			done();
		});

		it('should go to edit view through edit item button and check items', function(done) {
			helper.clickElement('.edit-prod1Name');
			browser.switchTo().defaultContent();
			helper.clickElement('#dataButton');
			helper.wait(5000);
			browser.switchTo().frame(0);
			helper.wait(5000);
			helper.checkElementToBe('#locationBreadcrumb', 'Edit Item');
			helper.checkValueToContain('#name-prod1Name', 'prod1Name', true);
			helper.checkValueToContain('#cat-prod1Name', 'prod1Cat', true);
			helper.checkValueToContain('#desc-prod1Name', 'prod1Desc', true);
			helper.checkValueToContain('#price-prod1Name', '100', true);
			helper.checkValueToContain('#sku-prod1Name', '123', true);
			helper.checkValueToContain('#taxes-prod1Name', '21', true);
			helper.checkValueToContain('#currency-prod1Name', '€', true);
			helper.checkStyleToContain('#prod1Name-0', 'padding: 0px; background-image: url("https://firebasestorage.googleapis.com/v0/b/fir-ecommercetests.appspot.com/o/images%2F-KdPfbP6jr9rAfmrDARA%2Fimg1?alt=media&token=90aded89-cef5-463f-b22c-13e60561e3d8");', true)
			helper.checkStyleToContain('#prod1Name-1', 'padding: 0px; background-image: url("https://firebasestorage.googleapis.com/v0/b/fir-ecommercetests.appspot.com/o/images%2F-KdPfbP6jr9rAfmrDARA%2Fimg2?alt=media&token=e6bff1d9-6afd-46a3-a168-859164adf212");', true)
			done();
		});

		it('should show an error if there is no Firebase url in dataConfig', function(done) {
			browser.switchTo().defaultContent();
			browser.driver.navigate().refresh()
			helper.wait(2000);
			element(by.css('#urlInput')).clear()
			.then(function() {
				element(by.id('urlInput')).sendKeys(url);
				element(by.css('#dataToSend')).clear()
				.then(function() {
					element(by.id('dataToSend')).sendKeys(dataConfigNoFirebaseUrl);
					helper.clickElement('#submitButton');
					helper.wait(1000);
					helper.clickElement('#readyButton');
					helper.clickElement('#dataButton');
					helper.wait(5000);
					browser.switchTo().frame(0);
					helper.checkElementToBe('#firebaseError', 'Failed to connect to Firebase, check module configuration.');
					done();
				});
			});
		});

		it('should change view to items through the test enviroment', function(done) {
			browser.switchTo().defaultContent();
			browser.driver.navigate().refresh()
			helper.wait(2000);
			element(by.css('#urlInput')).clear()
			.then(function() {
				element(by.id('urlInput')).sendKeys(url);
				element(by.css('#dataToSend')).clear()
				.then(function() {
					element(by.id('dataToSend')).sendKeys(dataConfig);
					helper.clickElement('#submitButton');
					helper.wait(1000);
					helper.clickElement('#readyButton');
					helper.clickElement('#dataButton');
					helper.wait(5000);
					browser.switchTo().frame(0);
					helper.checkElementToBe('#itemsTitle', 'List of items saved into the database');
					done();
				});
			});
		});

		it('should add a new item', function(done) {

			browser.switchTo().frame(0);

			helper.clickElement('#btnAddItem');
			browser.switchTo().defaultContent();
			helper.wait(3000);
			helper.clickElement('#dataButton');
			helper.wait(5000);
			browser.switchTo().frame(0);

			element(by.css('.nameInput')).sendKeys('prod3Name');
			element(by.css('.catInput')).sendKeys('prod3Cat');
			element(by.css('.descInput')).sendKeys('prod3Desc');
			element(by.css('.priceInput')).sendKeys(300);
			element(by.css('.skuInput')).sendKeys('125');
			element(by.css('.taxesInput')).sendKeys(25);
			setImageToUpload('snapseed-5.jpg', '#imageInput');

			helper.clickElement('#saveButton');

			helper.checkElementToBe('#successSave', 'New item added to the database');

			helper.clickElement('#itemsButton');
			browser.switchTo().defaultContent();
			helper.wait(3000);
			helper.clickElement('#dataButton');
			helper.wait(5000);
			browser.switchTo().frame(0);

			helper.wait(5000);
			helper.checkStyleToContain('.url-prod3Name', 'background-image: url("https://firebasestorage.googleapis.com/v0/b/fir-ecommercetests.appspot.com/o/images%2F', true)
			helper.elementExist('.url-prod3Name');
			helper.checkElementToBe('.name-prod3Name', 'prod3Name');
			helper.checkElementToBe('.sku-prod3Name', '125');
			helper.checkElementToBe('.cat-prod3Name', 'prod3Cat');
			helper.checkElementToBe('.price-prod3Name', '300$');

			done();
		});

		it('show an error message if there are fields without data', function(done) {
			helper.clickElement('#btnAddItem');
			browser.switchTo().defaultContent();
			helper.clickElement('#dataButton');
			helper.wait(5000);
			browser.switchTo().frame(0);
			helper.clickElement('#saveButton');
			helper.wait(2000);
			helper.checkElementToBe('#errorVal', 'All fields must be filled');
			done();
		});

		it('show an error message if the sku is a duplicate', function(done) {

			element(by.css('.nameInput')).sendKeys('prod3Name');
			element(by.css('.catInput')).sendKeys('prod3Cat');
			element(by.css('.descInput')).sendKeys('prod3Desc');
			element(by.css('.priceInput')).sendKeys(300);
			element(by.css('.skuInput')).sendKeys('123');
			element(by.css('.taxesInput')).sendKeys(25);

			setImageToUpload('snapseed-5.jpg', '#imageInput');
			helper.clickElement('#saveButton');
			helper.wait(2000);
			helper.checkElementToBe('#errorVal', 'The entered sku code is already associated in the database with another product');
			done();
		});

		it('should delete an image from edit view clicking on the image', function(done) {
			helper.clickElement('#itemsButton');
			browser.switchTo().defaultContent();
			helper.clickElement('#dataButton');
			helper.wait(5000);
			browser.switchTo().frame(0);
			helper.clickElement('.edit-prod3Name');
			browser.switchTo().defaultContent();
			helper.clickElement('#dataButton');
			helper.wait(5000);
			browser.switchTo().frame(0);
			helper.clickElement('#prod3Name-0')
			helper.isPresent('#prod3Name-0', false);
			done();
		});

		it('should delete an image using delete image button', function(done) {
			setImageToUpload('snapseed-5.jpg', '#imageInput');
			helper.wait(5000);
			helper.clickElement('#deleteImagesBtn')
			helper.isPresent('#prod3Name-0', false);
			done();
		});

		it('should edit a field', function(done) {
				helper.clickElement('#cancelButton');
				browser.switchTo().defaultContent();
				helper.clickElement('#dataButton');
				helper.wait(5000);
				browser.switchTo().frame(0);
				helper.clickElement('.edit-prod3Name');
				browser.switchTo().defaultContent();
				helper.clickElement('#dataButton');
				helper.wait(5000);
				browser.switchTo().frame(0);
				setImageToUpload('snapseed-5.jpg', '#imageInput');
				helper.wait(5000);
				element(by.css('#cat-prod3Name')).clear()
				.then(function() {
					element(by.id('cat-prod3Name')).sendKeys('prod3NewCat');
					helper.clickElement('#saveButton');
					helper.checkElementToBe('#successSave', 'Item changes added to the database');
					browser.switchTo().defaultContent();
					helper.clickElement('#dataButton');
					helper.wait(5000);
					browser.switchTo().frame(0);
					done();
				});
		});

		it('should check if the changes are saved', function(done) {
			helper.clickElement('#cancelButton');
			browser.switchTo().defaultContent();
			helper.clickElement('#dataButton');
			helper.wait(5000);
			browser.switchTo().frame(0);
			helper.checkElementToBe('.cat-prod3Name', 'prod3NewCat');
			done();
		});

		it('should go to edit view clicking into item image', function(done) {
			helper.clickElement('.goEdit-prod3Name');
			browser.switchTo().defaultContent();
			helper.clickElement('#dataButton');
			helper.wait(5000);
			browser.switchTo().frame(0);
			helper.checkValueToContain('#name-prod3Name', 'prod3Name', true);
			done();
		});

		it('should cancel an item deletion', function(done) {
			helper.clickElement('#cancelButton');
			browser.switchTo().defaultContent();
			helper.clickElement('#dataButton');
			helper.wait(5000);
			browser.switchTo().frame(0);
			helper.clickElement('.delete-prod3Name');
			helper.wait(2000);
			helper.clickElement('#cancelDelete');
			helper.checkElementToBe('.name-prod3Name', 'prod3Name');
			done();
		});

		it('should delete an item', function(done) {
			helper.clickElement('.delete-prod3Name');
			helper.wait(2000);
			helper.clickElement('#confirmDelete');
			helper.wait(5000);
			helper.isPresent('.name-prod3Name', false);
			helper.checkElementToBe('#successSave', 'Item has been successfully deleted');
			done();
		});

  });

	function setImageToUpload(fileName, inputName) {
		var fileToUpload = '../../images/'+fileName;
		var absolutePath = path.resolve(__dirname, fileToUpload);
		$(inputName).sendKeys(absolutePath);
	}

}());

# firebaseecommerce Module
===================================

![firebaseecommerce-popover](images/popover.png)

### Description

firebaseecommerce is the main module that you need to use to create a store inside your app. Concretely, ecommercelist allows you to display a list of all the products in stock and their price.


### Images
- [Logo](images/logo.png)
- [Screenshot](images/screenshot01.png)


### Details:

- Author: King of app
- Version: 1.0.5
- Homepage:

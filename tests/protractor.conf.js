exports.config = {
  seleniumAddress: 'http://localhost:4444/wd/hub',
  specs: ['/e2e/firebaseecommerce.e2e.test.js', '../widget/test/e2e/widget.e2e.test.js'],
  multiCapabilities: [{
    browserName: 'chrome'
  }],

};

(function() {
	'use strict';

	var helper = require('../../../../../e2e-tests/e2etest.service');

  describe('firebaseecommerce controller', function() {
    beforeAll(function() {
			browser.ignoreSynchronization = true;
      browser.driver.manage().window().setSize(400, 666);
      browser.get('http://localhost:9001/#/menu-abcd/firebaseecommerce-abcd');
    });

    it('should load module', function(done) {
      expect(element(by.css('.firebaseecommerce'))).toBeDefined();
      done();
    });

    it('should show the correct image', function(done) {
      helper.wait(5000);
      correctImageInsideKoa('.prod1Name', 'https://firebasestorage.googleapis.com/v0/b/fir-ecommercetests.appspot.com/o/images%2F-KdPfbP6jr9rAfmrDARA%2Fimg1?alt=media&token=90aded89-cef5-463f-b22c-13e60561e3d8');
      done(); });

    it('should show a list of items', function(done) {
      helper.elementExist('.prod1Name');
      done();
    });

    it('should show more than one item', function(done) {
      helper.elementExist('.prod2Name');
      done();
    });

    it('should show the price of the item', function(done) {
      helper.checkElementToBe('.price-prod1Name', '100');
      done();
    });

    it('should show the currency symbol of the item', function(done) {
      helper.checkElementToBe('.currency-prod1Name', '€');
      done();
    });

    it('should go to the firebaseecommeredetail module when an item is clicked', function(done) {
      helper.clickElement('.click-prod1Name');
			helper.checkUrl('/firebaseecommercedetail-abcd');
      done();
    });

    it('should show the correct image in detail view', function(done) {
      correctImageInsideKoa('.prod1Name-0', 'https://firebasestorage.googleapis.com/v0/b/fir-ecommercetests.appspot.com/o/images%2F-KdPfbP6jr9rAfmrDARA%2Fimg1?alt=media&token=90aded89-cef5-463f-b22c-13e60561e3d8');
      done();
    });

    it('should change the img when next button is pressed', function(done) {
      helper.clickElement('.next-prod1Name');
      helper.wait(1000);
      correctImageInsideKoa('.prod1Name-1', 'https://firebasestorage.googleapis.com/v0/b/fir-ecommercetests.appspot.com/o/images%2F-KdPfbP6jr9rAfmrDARA%2Fimg2?alt=media&token=e6bff1d9-6afd-46a3-a168-859164adf212');
      done();
    });

    it('should show the description of the item in detail view', function(done) {
      helper.checkElementToBe('.desc-prod1Name', 'prod1Desc');
      done();
    });

    it('should show the price of the item in detail view', function(done) {
      helper.checkElementToBe('.price-prod1Name', '100');
      done();
    });

    it('should show the currency symbol of the item in detail view', function(done) {
      helper.checkElementToBe('.currency-prod1Name', '€');
      done();
    });

    it('should appears the buy button', function(done) {
      helper.elementExist('.buyButton');
      done();
    });

  });

  function correctImageInsideKoa(elementId, expectedValue) {
    var parent = element(by.css(elementId));
    var child = parent.element(by.css('img'));
    child.getAttribute('src').then(function(value) {
      expect(value).toContain(expectedValue);
    });
  }

}());

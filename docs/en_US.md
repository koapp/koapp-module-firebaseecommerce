# (How to) Configure the module Firebase eCommerce

### How to configure?


Firebase eCommerce is a module that allows you to add a system of selling individual items in your app. The module generates a list of articles that show an image of the article, its name and price.
Each of the items are displayed in a **koa-card** element that can be clicked to access the detail of the item from which the purchase can be made.

From the detail of the article you can show more than one photo of the article as well as the name and description of it.

Firebase eCommerce works with **Firebase** databases.

In order to process sales, **Firebase eCommerce** needs a payment service. Currently **King of app** has the service **Paypal Service**, to add to the app from the **Builder**, that allows process sales through Paypal.

#### Configuring the database in Firebase

To configure the database in **Firebase** it is necessary to follow the following steps:

+ 1. Go to the official page of [Firebase](https://firebase.google.com).
+ 2. Click on the **Go to console** in the upper right corner.
![Go to console](https://s3-us-west-2.amazonaws.com/images.docs.kingofapp.com/firebaseecommerce/gotoconsole.png)
+ 3. Create a new **Firebase** project.
![Create new project](https://s3-us-west-2.amazonaws.com/images.docs.kingofapp.com/firebaseecommerce/firebaseNewProject.png)
+ 4. Click on *Add Firebase to your application*.
![Add Firebase to your app](https://s3-us-west-2.amazonaws.com/images.docs.kingofapp.com/firebaseecommerce/AddFirebaseToYourApp.png)
+ 5. Click on the 'Database' option in the left menu:
![Click on database](https://s3-us-west-2.amazonaws.com/images.docs.kingofapp.com/firebaseecommerce/gotodatabase.png)
+ 6. It´s necessary to create a collection to save the data of all the articles and another one for the sales. It´s recommended that they be called 'items' and 'sales' respectively but not mandatory:
![Creation of collections](https://s3-us-west-2.amazonaws.com/images.docs.kingofapp.com/firebaseecommerce/addcollectionname.png)
The 'items' collection is populated automatically by creating new articles from the administration section in the **Builder** although it could also be done manually but the data structure must be respected.
The 'sales' collection is also populated automatically when a sale is generated. It should not be modified manually.

#### Configuring access rules to our database in **Firebase**.

We must modify the access rules to the database and storage manually:

+ 1. Being into 'Database' option in the left menu of the **Firebase** console, select the 'RULES' tab:
![Go to rules](https://s3-us-west-2.amazonaws.com/images.docs.kingofapp.com/firebaseecommerce/rulestab.png)
+ 2. We will overwrite the access rules by deleting the ones that appear by default and leaving the ones shown in the following image:
![Overwrite rules for the database](https://s3-us-west-2.amazonaws.com/images.docs.kingofapp.com/firebaseecommerce/dbrules.png)
+ 3. Now we will overwrite the storage rules, selecting the 'Storage' option from the left menu:
![Overwrite rules for the database](https://s3-us-west-2.amazonaws.com/images.docs.kingofapp.com/firebaseecommerce/storagerules.png)
+ 4. In addition, we have to create an user and a password for the module administrator, so we go to the 'Authentication' option in the left menu:
![Create admin user](https://s3-us-west-2.amazonaws.com/images.docs.kingofapp.com/firebaseecommerce/gotoauth.png)
+ 5. From there, select the 'START-UP METHOD' tab:
![Go to start-up method](https://s3-us-west-2.amazonaws.com/images.docs.kingofapp.com/firebaseecommerce/gotomethod.png)
+ 6. From the list of providers, in email/password, we click where it says 'disabled', and we enable and save it:
![Enabling email](https://s3-us-west-2.amazonaws.com/images.docs.kingofapp.com/firebaseecommerce/allowmail.png)
+ 7. Finally, in the 'USERS' upper tab, we add a user that will correspond with the email and the password of the administrator, verifying that it appears later in the list of users:
![Add an user](https://s3-us-west-2.amazonaws.com/images.docs.kingofapp.com/firebaseecommerce/adduser.png)

#### Configuring the module **Firebase eCommerce**.

We must configure the module from the modules section of the **Builder**:

+ 1 We should access to the modules section of our application in the **Builder** and click on the **Firebase eCommerce** configuration:
![Go to module config](https://s3-us-west-2.amazonaws.com/images.docs.kingofapp.com/firebaseecommerce/accessedit.png)
+ 2 We must complete all the fields of the form, the email and the password will be those that we indicated when adding a user in our project of **Firebase** and the names of the collections correspond with as we have named them:
![Fields to fill](https://s3-us-west-2.amazonaws.com/images.docs.kingofapp.com/firebaseecommerce/allfields.png)
+ 3. To get the **Firebase** Api key from the console of our project of **Firebase**, we select the icon that represents a gear in the upper part of the left menu and in the dropdown we choose 'Configuration of the project':
![Go to project config](https://s3-us-west-2.amazonaws.com/images.docs.kingofapp.com/firebaseecommerce/gotoapikey.png)
+ 4. Copy the Api key and enter it in the configuration form of the **Builder**:
![Copy Api key](https://s3-us-west-2.amazonaws.com/images.docs.kingofapp.com/firebaseecommerce/apikey.png)
+ 5. The url database is obtained from the 'Database' option in the left menu:
![Copy the url from the database](https://s3-us-west-2.amazonaws.com/images.docs.kingofapp.com/firebaseecommerce/dburl.png)
+ 6. The url storage is obtained from the 'Storage' option in the left menu:
![Copy Storage url](https://s3-us-west-2.amazonaws.com/images.docs.kingofapp.com/firebaseecommerce/storageurl.png)
+ 7. We save the changes and we will have finished the module configuration **Firebase eCommerce**.

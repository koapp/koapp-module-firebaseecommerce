# (How to) Configurar el módulo Firebase eCommerce

### ¿Cómo se configura?

Firebase eCommerce es un módulo que permite añadir un sistema de venta de artículos individuales en tu app. El módulo genera una lista de artículos que muestran una imagen del artículo, su nombre y precio.
Cada uno de los artículos se muestran en un elemento **koa-card** sobre el que se puede pulsar para acceder al detalle del artículo desde el cual se puede realizar la compra.

Desde el detalle del artículo se puede mostrar más de una foto del artículo así como el nombre y la descripción del mismo.

Firebase eCommerce trabaja con bases de datos de **Firebase**.

Para poder tramitar las ventas, **Firebase eCommerce** necesita un servicio de pago. Actualmente King of app dispone del servicio **Paypal Service**, que hay que añadir a la app desde el **Builder**, que permite
tramitar las ventas a través de Paypal.

#### Configurando la base de datos en Firebase

Para configurar la base de datos en **Firebase** es necesario seguir los siguientes pasos:

+ 1. Ir a la página oficial de [Firebase](https://firebase.google.com).
+ 2. Hacer click en el botón de **Ir a la consola** en la esquina superior derecha:
![Ir a la consola](https://s3-us-west-2.amazonaws.com/images.docs.kingofapp.com/firebaseecommerce/gotoconsole.png)
+ 3. Crear un nuevo proyecto de **Firebase**:
![Crear nuevo proyecto](https://s3-us-west-2.amazonaws.com/images.docs.kingofapp.com/firebaseecommerce/firebaseNewProject.png)
+ 4. Hacemos click en *Agregar Firebase a tu aplicación*:
![Agregar Firebase a tu aplicación](https://s3-us-west-2.amazonaws.com/images.docs.kingofapp.com/firebaseecommerce/AddFirebaseToYourApp.png)
+ 5. Hacemos click en la opción 'Database' del menú izquierdo:
![Click en database](https://s3-us-west-2.amazonaws.com/images.docs.kingofapp.com/firebaseecommerce/gotodatabase.png)
+ 6. Es necesario crear una colección para guardar los datos de todos los artículos y otra para las ventas. Se recomienda que se llamen 'items' y 'sales' respectivamente pero no es obligatorio:
![Creación de las colecciones](https://s3-us-west-2.amazonaws.com/images.docs.kingofapp.com/firebaseecommerce/addcollectionname.png)
La colección 'items' se puebla automáticamente al crear nuevos artículos desde la sección de administración en el **Builder** aunque también podría realizarse manualmente pero se debe respetar la estructura de datos.
La colección 'sales' se puebla también de forma automática al generarse una venta. No debería ser modificada manualmente.

#### Configurando las reglas de acceso a nuestra base de datos en **Firebase**.

Debemos modificar las reglas de acceso a la base de datos y al storage de manera manual:

+ 1. Estando situados en la opción 'Database' del menú izquierdo de la consola de **Firebase**, seleccionamos la pestaña 'REGLAS':
![Ir a Reglas](https://s3-us-west-2.amazonaws.com/images.docs.kingofapp.com/firebaseecommerce/rulestab.png)
+ 2. Vamos a sobreescribir las reglas de acceso borrando las que aparecen por defecto y dejando las que se muestran en la siguiente imagen:
![Sobreescribir las reglas para la base de datos](https://s3-us-west-2.amazonaws.com/images.docs.kingofapp.com/firebaseecommerce/dbrules.png)
+ 3. Ahora vamos a sobreescribir las reglas del almacenamiento, seleccionando la opción 'Storage' del menú de la izquierda:
![Sobreescribir las reglas para el storage](https://s3-us-west-2.amazonaws.com/images.docs.kingofapp.com/firebaseecommerce/storagerules.png)
+ 4. Además, tenemos que crear un usuario y contraseña para el administrador del módulo, así que nos dirigimos a la opción 'Authentication' en el menú izquierdo:
![Creación del usuario admin](https://s3-us-west-2.amazonaws.com/images.docs.kingofapp.com/firebaseecommerce/gotoauth.png)
+ 5. Desde ahí, seleccionamos la pestaña 'MÉTODO DE INICIO DE SESIÓN':
![Ir a método de inicio](https://s3-us-west-2.amazonaws.com/images.docs.kingofapp.com/firebaseecommerce/gotomethod.png)
+ 6. De la lista de proveedores, en correo electrónico/ contraseña, hacemos click donde pone 'inhabilitado', lo habilitamos y guardamos:
![Habilitando email](https://s3-us-west-2.amazonaws.com/images.docs.kingofapp.com/firebaseecommerce/allowmail.png)
+ 7. Por último, en la pestaña superior 'USUARIOS', añadimos un usuario que corresponderá con el correo electrónico y la contraseña del administrador, comprobando que aparezca posteriormente en la lista de usuarios:
![Agregar usuario](https://s3-us-west-2.amazonaws.com/images.docs.kingofapp.com/firebaseecommerce/adduser.png)

#### Configurando el módulo **Firebase eCommerce**.

Debemos configurar el módulo desde la la sección de módulos del **Builder**

+ 1 Accedemos a la sección módulos de nuestra app en el **Builder** y hacemos click en el icono del módulo **Firebase eCommerce** para acceder a la configuración:
![Ir a la configuración del módulo](https://s3-us-west-2.amazonaws.com/images.docs.kingofapp.com/firebaseecommerce/accessedit.png)
+ 2 Debemos completar todos los campos del formulario, el email y la contraseña serán los que hemos indicado al añadir un usuario en nuestro proyecto de **Firebase** y los nombres de las colecciones corresponderán con como las hemos nombrado:
![Campos para rellenar](https://s3-us-west-2.amazonaws.com/images.docs.kingofapp.com/firebaseecommerce/allfields.png)
+ 3. Para obtener la Api key de **Firebase** desde la consola de nuestro proyecto de **Firebase** seleccionamos el icono que representa un engranaje en la parte superior del menú izquierdo y en el desplegable elegimos  'Configuración del proyecto':
![Ir a configuración del proyecto](https://s3-us-west-2.amazonaws.com/images.docs.kingofapp.com/firebaseecommerce/gotoapikey.png)
+ 4. Copiamos la Api key y la introducimos en el formulario de configuración del **Builder**:
![Copiar api key](https://s3-us-west-2.amazonaws.com/images.docs.kingofapp.com/firebaseecommerce/apikey.png)
+ 5. La url de la base de datos se obtiene desde la opción 'Database' del menú de la izquierda:
![Copiar la url de la base de datos](https://s3-us-west-2.amazonaws.com/images.docs.kingofapp.com/firebaseecommerce/dburl.png)
+ 6. La url del storage se obtiene desde la opción 'Storage' del menú de la izquierda:
![Copiar la url del storage](https://s3-us-west-2.amazonaws.com/images.docs.kingofapp.com/firebaseecommerce/storageurl.png)
+ 7. Guardamos los cambios y habremos finalizado la configuración del módulo **Firebase eCommerce**.

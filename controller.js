(function() {
'use strict';

angular
.module('firebaseecommerce', [])
.controller('firebaseecommerceController', firebaseecommerceController);

  firebaseecommerceController.$inject = ['$q', '$scope', '$location', '$rootScope', 'structureService', 'firebaseConnectionService', 'manageFirebaseService', 'firebaseStorageService'];

  function firebaseecommerceController($q, $scope, $location, $rootScope, structureService, firebaseConnectionService, manageFirebaseService, firebaseStorageService) {

    structureService.registerModule($location, $scope, 'firebaseecommerce');

    var moduleConfig = $scope.firebaseecommerce.modulescope;
    var promisesData = [];
    var promises     = [];

    init();

    function init() {
      $scope.isBusy            = true;
      $scope.showDataError     = false;
      $scope.showEmptyDatabase = true;
      structureService.launchSpinner('.transitionloader');
      var configObj = {
          apiKey        : moduleConfig.firebaseApiKey,
          databaseURL   : moduleConfig.firebaseUrl,
          storageBucket : moduleConfig.firebaseStorage,
          firebaseAuth  : moduleConfig.firebaseAuth,
          email         : moduleConfig.adminUser,
          password      : moduleConfig.adminPsswd
      };
      getsearchQuery();
      firebaseConnectionService.setFirebaseSource(configObj)
      .then(getFirebaseData)
      .catch(errorLog);
    }

    function getsearchQuery(){
      if($location.search().q)$scope.searchQuery = $location.search().q;
    }

    function getFirebaseData() {
      manageFirebaseService.getAll(moduleConfig.itemsCollection)
      .then(recoverImageUrls)
      .catch(errorLog);
    }

    function recoverImageUrls(data) {
      angular.forEach(data, addUrls);

      angular.forEach(data, function(value, key) {
        searchInsideItems(value, key, data)
      });

      $q.all(promisesData)
      .then(createItemsObject);

    }

    function searchInsideItems(value, key, data) {
      $scope.showEmptyDatabase = false;
      var defer                = $q.defer();

      angular.forEach(value.images, prepareImgArray);

      $q.all(promises)
      .then(function(urls) {
        data[key].images = urls;
        defer.resolve(data);
      });

      promisesData.push(defer.promise);
    }

    function createItemsObject(data) {
      $scope.items  = data[0];
      render();
      $scope.isBusy = false;
    }

    function addUrls(item) {
      item.url = moduleConfig.childrenUrl.firebaseecommercedetail + "?id=" + item.$id;
    }

    function prepareImgArray(value, key) {
      var deferred = $q.defer();
      firebaseStorageService.getImages(value)
      .then(function(url) {
        deferred.resolve(url);
      })
      .catch(function() {
        deferred.reject();
      });
      promises.push(deferred.promise);
      promises = [];
    }

    function errorLog(err) {
      console.log('Error', err);
      $scope.showEmptyDatabase = false;
      $scope.showDataError     = true;
      $scope.isBusy            = false;
    }

    function render() {
      $rootScope.$broadcast("renderKoaElements", {});
    }
  }
}());

angular.module('firebase.services', ['firebase'])
.service('firebaseConnectionService', firebaseConnectionService);

firebaseConnectionService.$inject = ['$firebaseArray', '$firebaseObject', '$q', '$firebaseAuth'];

function firebaseConnectionService($firebaseArray, $firebaseObject, $q, $firebaseAuth) {

  var firebaseFunctions = {
    setFirebaseSource : setFirebaseSource,
    getFirebaseRef    : getFirebaseRef,
    getStorageRef     : getStorageRef
  };

  function setFirebaseSource(configObj) {
    var config = {
        apiKey: configObj.apiKey,
        authDomain: configObj.authDomain,
        databaseURL: configObj.databaseURL,
        storageBucket: configObj.storageBucket,
    };

    var q = $q.defer();

    if(firebase && firebase.apps.length == 0) {
      try {
        firebase.initializeApp(config);
        firebaseSignOut();
      }
      catch(err) {
        q.reject(err);
      }
    } else {
      q.resolve();
    }

    function firebaseSignOut() {
      firebase.auth().signOut().then(function() {
        authenticateUser();
        q.resolve();
      }, function(err) {
        console.log('err -> ', err);
      });
    }

    function authenticateUser() {
      firebase.auth().signInWithEmailAndPassword(configObj.email, configObj.password)
      .catch(errorLog);
    }

    function errorLog(error) {
      console.log('Error -> ', error);
    }

    return q.promise;

  }

  function getFirebaseRef(collection) {
    return firebase.database().ref(collection);
  }

  function getStorageRef() {
    return firebase.storage().ref();
  }

  return firebaseFunctions;

}

angular.module('firebase.services')
.service('salesService', salesService);

salesService.$inject = ['$firebaseArray', '$firebaseObject', '$q', '$firebaseAuth', 'firebaseConnectionService'];

function salesService($firebaseArray, $firebaseObject, $q, $firebaseAuth, firebaseConnectionService) {

  var functions = {
    pushSale : pushSale
  }

  function pushSale(saleToPush, salesCollection) {

    var firebaseRef  = firebaseConnectionService.getFirebaseRef(salesCollection);

    var newPostRef  = firebaseRef.push(salesCollection + '/');
    var imagesPath  = {};
    var currentDate = moment().format('MMMM Do YYYY, h:mm:ss a');

    var dataToCreate = {
      'sku'            : saleToPush.sku,
      'name'           : saleToPush.name,
      'price'          : saleToPush.price,
      'currencySymbol' : saleToPush.currencySymbol,
      'image'          : saleToPush.images[0],
      'date'           : currentDate
    }
    newPostRef.set(dataToCreate);
  }

  return functions;

}

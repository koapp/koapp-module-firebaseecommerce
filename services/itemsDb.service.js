angular.module('firebase.services')
.service('catalogService', catalogService);

catalogService.$inject = ['$firebaseArray', '$firebaseObject', '$q', '$firebaseAuth', 'firebaseConnectionService',
                          'firebaseStorageService', 'manageFirebaseService'];

function catalogService($firebaseArray, $firebaseObject, $q, $firebaseAuth, firebaseConnectionService,
                        firebaseStorageService, manageFirebaseService) {

  var functions = {
    checkRecord : checkRecord,
    removeItem  : removeItem,
    setItem     : setItem,
    pushItem    : pushItem
  }

  function checkRecord(dataReceived, itemsCollection) {
    var q            = $q.defer();
    var recordExists = false;
    firebaseRef      = firebaseConnectionService.getFirebaseRef(itemsCollection);
    var items        = $firebaseArray(firebaseRef);
    items.$loaded()
    .then(recordManage);
    return q.promise;

    function recordManage(data) {
      for (var key in data) {
         (data[key].sku === dataReceived.sku) ?  recordExists = true : false;
      }
      (recordExists) ? q.resolve(true) : q.reject(false);
    }
  }

  function removeItem(idToDelete, itemsCollection) {
    var mainQ = $q.defer();

    manageFirebaseService.getSingleItemData(idToDelete, itemsCollection)
    .then(deleteFromDb)
    .then(deleteItems)
    .catch(errorLog);

    function deleteFromDb(itemData) {
      var images = angular.copy(itemData.images);
      var q = $q.defer();
      refToDb           = firebaseConnectionService.getFirebaseRef(itemsCollection).child(idToDelete);
      var itemsToDelete = $firebaseObject(refToDb);

      itemsToDelete.$remove()
      .then(function resolvePromise(ref) {
        q.resolve(images);
      },
      function rejectPromise() {
        mainQ.reject(true);
        q.reject(false);
      });

      return q.promise;
    }

    function deleteItems(imagesToDelete) {
      var promises  = [];

      $q.all(promises)
      .then(function() {
        mainQ.resolve(true);
      })
      .catch(errorLog);

      angular.forEach(imagesToDelete, deleteFromStorage);

      function deleteFromStorage(imagesPath) {
        var deferred   = $q.defer();
        storageRef     = firebaseConnectionService.getStorageRef();
        var imgStorage = storageRef.child(imagesPath);
        imgStorage.delete()
        .then(function() {
          deferred.resolve(true);
        })
        .catch(function() {
          mainQ.reject(true);
          deferred.reject(false);
        });
        promises.push(deferred.promise);
      }
    }

    return mainQ.promise;
  }

  function setItem(itemToPush, itemsCollection) {
    var q = $q.defer();
    path  = itemsCollection + '/' + itemToPush.$id;

    var dataToUpdate = {
      'name'           : itemToPush.name,
      'category'       : itemToPush.category,
      'price'          : itemToPush.price,
      'currencySymbol' : itemToPush.currencySymbol,
      'sku'            : itemToPush.sku,
      'images'         : itemToPush.images,
      'taxes'          : itemToPush.taxes,
      'description'    : itemToPush.description
    };

    firebase.database().ref(path).set(dataToUpdate)
    .then(pushSuccess)
    .catch(pushFail);

    function pushSuccess() {
      q.resolve(true);
    }

    function pushFail() {
      q.reject(false);
    }

    return q.promise;
  }

  function pushItem(itemToPush, itemsCollection, images64) {
    var q           = $q.defer();
    var firebaseRef = firebaseConnectionService.getFirebaseRef(itemsCollection)
    var newPostRef  = firebaseRef.push(itemsCollection + '/');
    var imagesPath  = {};
    var newId       = newPostRef.path['o'][1];

    angular.forEach(images64, createImagesPath);

    var dataToCreate = {
      'name'           : itemToPush.name,
      'category'       : itemToPush.category,
      'price'          : itemToPush.price,
      'currencySymbol' : itemToPush.currencySymbol,
      'images'         : imagesPath,
      'sku'            : itemToPush.sku,
      'taxes'          : itemToPush.taxes,
      'description'    : itemToPush.description
    }
    newPostRef.set(dataToCreate)
    .then(function() {
      pushImagesPush(newId, images64);
    })
    .catch(pushFail);

    function pushImagesPush(newId, images64) {
      firebaseStorageService.pushImages(newId, images64)
      .then(pushSuccess)
      .catch(pushFail);
    }

    function pushSuccess() {
      q.resolve(true);
    }

    function pushFail() {
      q.reject(false);
    }

    function createImagesPath(value, key) {
        imagesPath[key] = 'images/' + newId + '/' + key;
    }

    return q.promise;
  }

  function errorLog(error) {
    console.log('Error -> ', error);
  }

  return functions;

}

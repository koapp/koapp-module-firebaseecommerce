angular.module('firebase.services')
.service('manageFirebaseService', manageFirebaseService);

manageFirebaseService.$inject = ['$firebaseArray', '$firebaseObject', '$q', '$firebaseAuth', 'firebaseConnectionService'];

function manageFirebaseService($firebaseArray, $firebaseObject, $q, $firebaseAuth, firebaseConnectionService) {

  var functions = {
    getAll            : getAll,
    getSingleItemData : getSingleItemData
  }

  var firebaseRef  = '';
  var dataReceived = [];

  function getAll(collection) {
    var q = $q.defer();
    try {
      firebaseRef  = firebaseConnectionService.getFirebaseRef(collection);
      dataReceived = $firebaseArray(firebaseRef);
      dataReceived.$loaded().then(function(data) {
        q.resolve(data);
      })
      .catch(errorLog);
    }
    catch(err) {
      q.reject(err);
    }
    return q.promise;
  }

  function getSingleItemData(idToGet, collection) {
    var q       = $q.defer();
    firebaseRef = firebaseConnectionService.getFirebaseRef(collection).child(idToGet);
    items       = $firebaseObject(firebaseRef);
    items.$loaded().then(function(data) {
      q.resolve(data);
    });
    return q.promise;
  }

  function errorLog(error) {
    console.log('Error -> ', error);
  }

  return functions;

}

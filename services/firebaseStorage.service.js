angular.module('firebase.services')
.service('firebaseStorageService', firebaseStorageService);

firebaseStorageService.$inject = ['$firebaseArray', '$firebaseObject', '$q', '$firebaseAuth', 'firebaseConnectionService'];

function firebaseStorageService($firebaseArray, $firebaseObject, $q, $firebaseAuth, firebaseConnectionService) {

  var functions = {
    pushImages : pushImages,
    getImages  : getImages
  }

  var storageRef = '';

  function pushImages(id, images64) {
    var q = $q.defer();
    try {
      var storageRef = firebase.storage().ref();
      angular.forEach(images64, createImagesToUpload);
      q.resolve(true);
    }
    catch(err) {
      q.reject(false);
    }

    return q.promise;

    function createImagesToUpload(value, key) {
      if(!value.url.startsWith('http')) {
        var imagePath   = 'images/' + id + '/' + key;
        var imagesRef   = storageRef.child(imagePath);
        var base64Array = value.url.split('base64,');
        var uploadTask  = imagesRef.putString(base64Array[1], 'base64');
      }

    }
  }

  function getImages(imagePath) {
    var q          = $q.defer();
    storageRef     = firebaseConnectionService.getStorageRef();
    var imgRef     = storageRef.child(imagePath);
    var result     = imgRef.getMetadata()
    .then(function(metadata) {
      q.resolve(metadata.downloadURLs[0]);
    })
    .catch(function() {
      q.reject();
    });
    return q.promise;
  }

    return functions;

  }
